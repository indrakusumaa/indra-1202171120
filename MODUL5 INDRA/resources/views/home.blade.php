@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <img src="indra.jpg" alt="" style="width : 50px; height: 50px" class="rounded-circle"> &nbsp;{{ Auth::user()->name }}</div>

                <div class="card-body">
                    <img src="telkom.jpg"  style="width: 500px;">
                </div>
                <div class="card-footer">
                <b>{{ Auth::user()->email }}</b>
                <br>
                Gedung Tokong Nanas 
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
