<?php

  $order = $_POST['order'];
  $pemesan = $_POST['pemesan'];
  $alamat = $_POST['alamat'];
  $email = $_POST['email'];
  $member = $_POST['member'];
  $metode = $_POST['metodepembayaran'];
  $Total = 0;

  for ($i=1; $i <= 5; $i++) {
    if (isset($_POST['kanan'.$i])) {
      $hitung = $_POST['kanan'.$i];
    }else {
      $hitung = 0;
    }
    $Total += $hitung;
  }
  if ($member == "member") {
    $Totharga = $Total*0.9;
  }else {
    $Totharga = $Total;
  }
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    
  </head>
  <body>
    <center>
    <h2 class="mt-5">TRANSAKSI <br>PEMESANAN</h2>
    <h6>Terimakasih telah berbelanja di Kopi Susu Duarr!</h6>
    <?php echo "Rp. $Totharga ,-";?>
    
     <table width="300px" height="500px" rules="rows">
       <tr>
         <td>ID</td>
         <td><?= $order ?></td>
       </tr>
       <tr>
         <td>Nama</td>
         <td><?= $pemesan ?></td>
       </tr>
       <tr>
         <td>Email</td>
         <td><?= $email ?></td>
       </tr>
       <tr>
         <td>Alamat</td>
         <td><?= $alamat ?></td>
       </tr>
       <tr>
         <td>Member</td>
         <td><?= $member ?></td>
       </tr>
       <tr>
         <td>Pembayaran</td>
         <td><?= $metode ?></td>
       </tr>
     </table>
    </center>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>