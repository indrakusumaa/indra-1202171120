
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="home.css">

    <title></title>
  </head>
  <body>
        <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
          <img src="ead.PNG" alt="logo " style="width: 120px;">  
        </a>
        <div class="d-flex flex-row-reverse bd-highlight">
            <a class="navbar-text" href="#" style="padding: 20px;" data-toggle="modal" data-target="#Register">Register</a>
            <a class="navbar-text" href="#" style="padding: 20px;" data-toggle="modal" data-target="#Login">Login</a>
        </div>
    </nav>

  <div class="jumbotron jumbotron-fluid">
  <div class="container" style=" background-color: #00FFFF; padding: 50px;">
    <h1 class="display-4">Hello Coders!</h1>
    <p class="lead">Welcome to our store,please take a look for the products you may buy.</p>
  </div>
</div>

<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-4">
      <div class="card" style="width: 350px; height: 550px">
        <img src="internet.png" style="width: 350px" class="card-img-top" alt="...">
          <div class="card-body">
            <h5 class="card-title">Learning Basic Web Programming</h5>
                <h6 class="card-title">Rp.210.000</h6>
                  <p class="card-text">Want to be able to make a website? Learn basic components such as HTML, CSS and JavaScript in this class curriculum.</p>
                    <a href="#" class="btn btn-primary">Buy</a>
          </div>
      </div>
    </div>  
  <div class="col-md-4">
    <div class="card" style="width: 350px; height: 550px; ">
  <img src="java.jpg" style="width: 350px" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">Starting Programming in Java</h5>
      <h6 class="card-title">Rp.150.000</h6>
        <p class="card-text">Learn java language for you who want to learn the most popular Object-Oriented-Programming(PBO) concepts for developing applications</p>
        <a href="#" class="btn btn-primary">Buy</a>
  </div>
</div>
  </div>
    <div class="col-md-4">
      <div class="card" style="width: 350px ; height: 550px;">
        <img src="phyton.png" style="width: 350px" class="card-img-top" alt="...">
          <div class="card-body">
    <h5 class="card-title">Card title</h5>
        <h6 class="card-title">Rp.200.000</h6>
          <p class="card-text">Learn Phyton - Fundamental various current industry trends. Data Science, Machine Learning, Infrastructure Management.</p>
          <a href="#" class="btn btn-primary">Buy</a>
  </div>
</div>
  </div>

  </div>
  
</div>
<!--Modal Register-->
<div class="modal" tabindex="-1" role="dialog" id="Register">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Register</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form class="" action="" method="post">
                <label>Email address</label><br>
                <input type="email" class="form-control form-control-sm" value="" placeholder="Enter Email" required name="email"><br>
                <label>Username</label><br>
                <input type="text" class="form-control form-control-sm" value="" placeholder="Enter Username" required name="username"><br>
                <label>Mobile Number</label><br>
                <input type="number" class="form-control form-control-sm" value="" placeholder="Enter Mobile Number" required name="nohp"><br>
                <label>Password</label><br>
                <input type="password" class="form-control form-control-sm" value="" placeholder="Password" required name="password"><br>
                <label>Confirm Password</label><br>
                <input type="password" class="form-control form-control-sm" value="" placeholder="Confirm Password" required name="confpass"><br>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
              <button type="submit" name="submit" class="btn btn-primary">Register</button>
            </form>
      </div>
    </div>
  </div>
</div>

<!--Modal Login-->
<div class="modal" tabindex="-1" role="dialog" id="Login">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="" action="login.php" method="post">
                  <label>Username</label><br>
                  <input type="username" class="form-control form-control-sm" value="" placeholder="Enter Email" required name="username"><br>
                  <label>Password</label><br>
                  <input type="password" class="form-control form-control-sm" value="" placeholder="Password" required name="pass"><br>
              </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                <button type="submit" name="login" class="btn btn-primary">Login</button>
        </form>
    </div>
  </div>
</div>
</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>