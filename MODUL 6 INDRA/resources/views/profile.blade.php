@extends('layouts.app')
@section('content')

<div class="container">
    <div class="row">
        <div class="col-3 p-5">
            <img src="indra.jpg" class="rounded-circle w-100" height="200px">
        </div>
        
        <div class="col-9 pt-5">
        <div class="d-flex justify-content-between align-items-baseline">
        <div class="d-flex align-items-center pb-3">
        <div><h1>{{ Auth::user()->name }}</h1></div>
        <div class="h4"></div>
        </div>
        <a href="{{url('/addpost')}}">Add New Post</a>
        </div>
        <a href="{{url('/editprofile')}}">Edit Profile</a>

            <div class="d-flex">
                <div class="pr-5"><strong>0</strong> Post</div>
            </div>

            <div class="pt-4 font-weight-bold"></div>
            <div></div>
            <div><a href="#"></a></div>
        </div>
        </div>

    </div>
    <div class="row pt-5"> 
        <div class="col-4 pt-4">
        <a href="">
            <img src="" class="w-100">
        </a>
        </div>
        </div>
        
        
</div>

@endsection